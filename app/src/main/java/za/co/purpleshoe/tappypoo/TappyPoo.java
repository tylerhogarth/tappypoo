package za.co.purpleshoe.tappypoo;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import io.fabric.sdk.android.Fabric;
import za.co.purpleshoe.tappypoo.util.Lawg;

/**
 * Created by Tyler Hogarth on 16/06/20.
 */
public class TappyPoo extends Application {

    public static boolean PROD_MODE = false;

    private Tracker tracker;

    @Override
    public void onCreate() {
        super.onCreate();

        if (getResources().getBoolean(R.bool.isProd)) {
            PROD_MODE = true;
            Fabric.with(this, new Crashlytics());
        } else {
            Lawg.DEV_MODE = true;
        }
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (tracker == null) {
            GoogleAnalytics analytics = GoogleAnalytics.getInstance(this);
            // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
            tracker = analytics.newTracker(R.xml.global_tracker);
        }
        return tracker;
    }

    public static Analytics initAnalytics(Application app) {
        return ((TappyPoo) app).createAnalytics();
    }

    public Analytics createAnalytics()  {
        return new Analytics();
    }

    public class Analytics {

        public static final String SCREEN_MAIN = "Main";

        public static final String CATEGORY_USER_ACTION = "User Action";
        public static final String CATEGORY_ACHIEVMENT = "Achievement";

        public static final String ACTION_OPTION_SELECT = "Option";
        public static final String ACTION_LEVEL_SELECT = "Level";
        public static final String ACTION_TOP_SCORE = "Top Score";

        public static final String LABEL_OPTION_MUTE = "Mute";
        public static final String LABEL_OPTION_UNMUTE = "Unmute";
        public static final String LABEL_OPTION_SETTINGS = "Settings";

        public static final String LABEL_LEVEL_SURVIVAL = "Lvl Survival";
        public static final String LABEL_LEVEL_EASY = "Lvl Easy";
        public static final String LABEL_LEVEL_HARD = "Lvl Hard";

        public static final String LABEL_TOP_SCORE_SURVIVAL = "Scr Survival";
        public static final String LABEL_TOP_SCORE_EASY = "Scr Easy";
        public static final String LABEL_TOP_SCORE_HARD = "Scr Hard";

        public void sendScreen(String screenName) {
            if (PROD_MODE) {
                getDefaultTracker().setScreenName(screenName);
                getDefaultTracker().send(new HitBuilders.ScreenViewBuilder().build());
            }
        }

        public void sendEvent(String category, String action) {
            if (PROD_MODE) {
                getDefaultTracker().send(new HitBuilders.EventBuilder()
                        .setCategory(category)
                        .setAction(action)
                        .build());
            }
        }

        public void sendEvent(String category, String action, String label) {
            if (PROD_MODE) {
                getDefaultTracker().send(new HitBuilders.EventBuilder()
                        .setCategory(category)
                        .setAction(action)
                        .setLabel(label)
                        .build());
            }
        }

        public void sendEvent(String category, String action, String label, long value) {
            if (PROD_MODE) {
                getDefaultTracker().send(new HitBuilders.EventBuilder()
                        .setCategory(category)
                        .setAction(action)
                        .setLabel(label)
                        .setValue(value)
                        .build());
            }
        }
    }
}
