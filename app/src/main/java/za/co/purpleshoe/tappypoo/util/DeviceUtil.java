package za.co.purpleshoe.tappypoo.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.provider.Settings;
import android.view.Display;

/**
 * Created by Tyler on 2015-10-26.
 */
public class DeviceUtil {

    public static int getScreenWidth(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    public static int getScreenHeight(Activity activity) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    /**
     * A 64-bit number (as a hex string) that is randomly generated when the user first sets up the
     * device and should remain constant for the lifetime of the user's device. The value may change
     * if a factory reset is performed on the device.
     *
     * Note: When a device has multiple users (available on certain devices running Android 4.2 or
     * higher), each user appears as a completely separate device, so the ANDROID_ID value is unique
     * to each user.
     *
     * @param context
     * @return
     */
    public static String getAndroidId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }
}
