package za.co.purpleshoe.tappypoo.activity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;

import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import za.co.purpleshoe.tappypoo.R;
import za.co.purpleshoe.tappypoo.TappyPoo;
import za.co.purpleshoe.tappypoo.tapgame.CircularSeekBar;
import za.co.purpleshoe.tappypoo.tapgame.TapGame;
import za.co.purpleshoe.tappypoo.tapgame.TapGameBasic;
import za.co.purpleshoe.tappypoo.ui.SettingsDialog;
import za.co.purpleshoe.tappypoo.util.Lawg;
import za.co.purpleshoe.tappypoo.util.UserPreferences;

public class MainActivity extends BaseActivity implements TapGameBasic.TapGameBasicListener, SettingsDialog.SettingsChangeListener {

    public static final int DIFFICULTY_EASY = 0;
    public static final int DIFFICULTY_NORMAL = 1;
    public static final int DIFFICULTY_HARD = 2;

    public static final int MOOD_FART = 0;
    public static final int MOOD_HAPPY = 1;
    public static final int MOOD_SAD = 2;

    private TapGameBasic tapGame;

    private boolean activityOpen = false;

    private int topScore = 0;
    private boolean topScoreBroken = false;
    private boolean playSound = true;

    private Bitmap poopRainbow;
    private Bitmap poopHappy;
    private Bitmap poopSad;
    private Bitmap poopLove;

    @Bind(R.id.options_top_score_tv)
    TextView topScoreTv;

    @Bind(R.id.activity_tap_game_ultra_parent_rl)
    RelativeLayout ultraParent;

    @Bind(R.id.activity_tap_game_sb_background)
    CircularSeekBar seekbarBackground;

    @Bind(R.id.activity_tap_game_sb_overlay)
    CircularSeekBar seekbarOverlay;

    @Bind(R.id.activity_tap_game_score_tv)
    TextView scoreTv;

    @Bind(R.id.activity_tap_game_click_listener)
    View tapperView;

    @Bind(R.id.activity_tap_game_logo_iv)
    ImageView logoIv;

    @Bind(R.id.options_volume_iv)
    ImageView volumeIv;

    @Override
    String getScreenName() {
        return TappyPoo.Analytics.SCREEN_MAIN;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        playSound = UserPreferences.getBoolean(this, UserPreferences.OPTION_PLAY_SOUND, true);
        setVolumeIcon(playSound);

        setGameBackground(false);
        tapGame = new TapGameBasic(this,
                seekbarOverlay,
                seekbarBackground);
        tapGame.setRotate(true);
        tapGame.setListener(this);

        poopRainbow = BitmapFactory.decodeResource(getResources(), R.drawable.ic_poop_rainbow);
        poopHappy = BitmapFactory.decodeResource(getResources(), R.drawable.ic_poop_happy);
        poopSad = BitmapFactory.decodeResource(getResources(), R.drawable.ic_poop_sad);
        poopLove = BitmapFactory.decodeResource(getResources(), R.drawable.ic_poop_love);

        switch (UserPreferences.getInteger(this, UserPreferences.DIFFICULTY_CURRENT, DIFFICULTY_NORMAL)) {
            case DIFFICULTY_EASY:
                tapGame.setDurationType(TapGame.Duration.SURVIVAL);
                tapGame.setDifficulty(TapGame.Difficulty.EASY);
                break;
            case DIFFICULTY_NORMAL:
                tapGame.setDurationType(TapGame.Duration.ARCADE);
                tapGame.setDifficulty(TapGame.Difficulty.NORMAL);
                break;
            case DIFFICULTY_HARD:
                tapGame.setDurationType(TapGame.Duration.ARCADE);
                tapGame.setDifficulty(TapGame.Difficulty.HARD);
                break;
        }

        setTopScore(-1);

        tapperView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                Lawg.i(event.getAction());
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    tapGame.tapTouch();
                }
                return true;
            }
        });

        tapGame.readyGame();
    }

    public void setGameBackground(boolean dead) {
        if (dead) {
            ultraParent.setBackgroundColor(getResources().getColor(R.color.grey_dark));
        } else {
            ultraParent.setBackgroundColor(getResources().getColor(R.color.primary));
        }
    }

    @OnClick(R.id.options_settings_iv)
    void settingsClick() {

        if (tapGame.isPlaying()) {
            return;
        }
        SettingsDialog settingsDialog = new SettingsDialog(this);
        settingsDialog.setOnSettingsChangeListener(this);
        settingsDialog.show();

        analytics.sendEvent(TappyPoo.Analytics.CATEGORY_USER_ACTION,
                TappyPoo.Analytics.ACTION_OPTION_SELECT,
                TappyPoo.Analytics.LABEL_OPTION_SETTINGS);
    }

    @OnClick(R.id.options_volume_iv)
    void volumeClick() {
        playSound = !playSound;
        UserPreferences.setBoolean(this, UserPreferences.OPTION_PLAY_SOUND, playSound);
        setVolumeIcon(playSound);

        if (playSound) {
            analytics.sendEvent(TappyPoo.Analytics.CATEGORY_USER_ACTION,
                    TappyPoo.Analytics.ACTION_OPTION_SELECT,
                    TappyPoo.Analytics.LABEL_OPTION_UNMUTE);
        } else {
            analytics.sendEvent(TappyPoo.Analytics.CATEGORY_USER_ACTION,
                    TappyPoo.Analytics.ACTION_OPTION_SELECT,
                    TappyPoo.Analytics.LABEL_OPTION_MUTE);
        }
    }

    public void setVolumeIcon(boolean playSound) {
        if (playSound) {
            volumeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_option_mute));
        } else {
            volumeIv.setImageDrawable(getResources().getDrawable(R.drawable.ic_option_volume));
        }
    }

    @Override
    public void onGameReady() {
        scoreTv.setText("0");
        setGameBackground(false);
        logoIv.setImageBitmap(poopRainbow);
    }

    @Override
    public void onGameStart() {
        logoIv.setImageBitmap(poopHappy);
    }

    @Override
    public void onGameEnd() {
        setGameBackground(true);
        saveTopScore();
        logoIv.setImageBitmap(poopSad);
        playSound(MOOD_SAD);
        topScoreBroken = false;
    }

    @Override
    public void onIncrementScore() {
        if (tapGame.getScore() > topScore) {
            setTopScore(tapGame.getScore());
            logoIv.setImageBitmap(poopLove);
            if (!topScoreBroken) {
                playSound(MOOD_HAPPY);
            } else {
                playSound(MOOD_FART);
            }
            topScoreBroken = true;
        } else {
            playSound(MOOD_FART);
        }
    }

    @Override
    public void onReverse() {

    }

    @Override
    public void postReverse() {
        scoreTv.setText(tapGame.getScore() + "");
    }

    public void saveTopScore() {

        if (tapGame.getScore() > topScore) {
            topScore = tapGame.getScore();
            switch (tapGame.getDifficulty()) {
                case EASY:
                    UserPreferences.setInteger(this, UserPreferences.SCORE_TOP_EASY, topScore);
                    analytics.sendEvent(TappyPoo.Analytics.CATEGORY_ACHIEVMENT,
                            TappyPoo.Analytics.ACTION_TOP_SCORE,
                            TappyPoo.Analytics.LABEL_TOP_SCORE_SURVIVAL,
                            topScore);
                    break;

                case NORMAL:
                    UserPreferences.setInteger(this, UserPreferences.SCORE_TOP_NORMAL, topScore);
                    analytics.sendEvent(TappyPoo.Analytics.CATEGORY_ACHIEVMENT,
                            TappyPoo.Analytics.ACTION_TOP_SCORE,
                            TappyPoo.Analytics.LABEL_TOP_SCORE_EASY,
                            topScore);
                    break;

                case HARD:
                    UserPreferences.setInteger(this, UserPreferences.SCORE_TOP_HARD, topScore);
                    analytics.sendEvent(TappyPoo.Analytics.CATEGORY_ACHIEVMENT,
                            TappyPoo.Analytics.ACTION_TOP_SCORE,
                            TappyPoo.Analytics.LABEL_TOP_SCORE_HARD,
                            topScore);
                    break;
            }
        }
    }

    public void setTopScore(int score) {

        String label = "";
        if (tapGame.getDifficulty() == TapGame.Difficulty.EASY) {

            if (score < 0) {
                topScore = UserPreferences.getInteger(this, UserPreferences.SCORE_TOP_EASY, 0);
                score = topScore;
            }
            label = getResources().getString(R.string.diff_easy);

        } else if (tapGame.getDifficulty() == TapGame.Difficulty.NORMAL) {

            if (score < 0) {
                topScore = UserPreferences.getInteger(this, UserPreferences.SCORE_TOP_NORMAL, 0);
                score = topScore;
            }
            label = getResources().getString(R.string.diff_normal);

        } else if (tapGame.getDifficulty() == TapGame.Difficulty.HARD) {

            if (score < 0) {
                topScore = UserPreferences.getInteger(this, UserPreferences.SCORE_TOP_HARD, 0);
                score = topScore;
            }
            label = getResources().getString(R.string.diff_hard);
        }

        topScoreTv.setText(getResources().getString(R.string.top_score_label)
                .replace("{:value}", score + "")
                .replace("{:label}", label));
    }

    @Override
    public void onDifficultyChange(TapGame.Difficulty difficulty) {

        if (difficulty == tapGame.getDifficulty()) {
            return;
        } else if (tapGame.getDifficulty() == TapGame.Difficulty.HARD) {
            tapGame.resetRotation(true);
        }

        if (difficulty == TapGame.Difficulty.EASY) {

            tapGame.setDurationType(TapGame.Duration.SURVIVAL);
            tapGame.setDifficulty(TapGame.Difficulty.EASY);
            UserPreferences.setInteger(this, UserPreferences.DIFFICULTY_CURRENT, DIFFICULTY_EASY);

            analytics.sendEvent(TappyPoo.Analytics.CATEGORY_USER_ACTION,
                    TappyPoo.Analytics.ACTION_LEVEL_SELECT,
                    TappyPoo.Analytics.LABEL_LEVEL_SURVIVAL);

        } else if (difficulty == TapGame.Difficulty.NORMAL) {

            tapGame.setDurationType(TapGame.Duration.ARCADE);
            tapGame.setDifficulty(TapGame.Difficulty.NORMAL);
            UserPreferences.setInteger(this, UserPreferences.DIFFICULTY_CURRENT, DIFFICULTY_NORMAL);

            analytics.sendEvent(TappyPoo.Analytics.CATEGORY_USER_ACTION,
                    TappyPoo.Analytics.ACTION_LEVEL_SELECT,
                    TappyPoo.Analytics.LABEL_LEVEL_EASY);

        } else if (difficulty == TapGame.Difficulty.HARD) {

            tapGame.setDurationType(TapGame.Duration.ARCADE);
            tapGame.setDifficulty(TapGame.Difficulty.HARD);
            UserPreferences.setInteger(this, UserPreferences.DIFFICULTY_CURRENT, DIFFICULTY_HARD);

            analytics.sendEvent(TappyPoo.Analytics.CATEGORY_USER_ACTION,
                    TappyPoo.Analytics.ACTION_LEVEL_SELECT,
                    TappyPoo.Analytics.LABEL_LEVEL_HARD);
        }
        setTopScore(-1);
        tapGame.readyGame();
    }

    public TapGameBasic getTapGame() {
        return tapGame;
    }

    public void playSound(int mood) {

        if (!playSound || !activityOpen) {
            return;
        }

        int raw = -1;
        switch (mood) {
            case MOOD_FART:
                raw = getFartRes();
                break;
            case MOOD_HAPPY:
                raw = getHappyRes();
                break;

            case MOOD_SAD:
                raw = getSadRes();
                break;
        }

        if (raw != -1) {
            MediaPlayer mediaPlayer = MediaPlayer.create(this, raw);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mediaPlayer.setLooping(false);
            mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.release();
                }
            });
            mediaPlayer.start();
        }
    }

    public int getHappyRes() {

        int rand = new Random().nextInt(7);

        switch (rand) {
            case 0:
                return R.raw.rw_poop_happy_01;
            case 1:
                return R.raw.rw_poop_happy_02;
            case 2:
                return R.raw.rw_poop_happy_03;
            case 3:
                return R.raw.rw_poop_happy_04;
            case 4:
                return R.raw.rw_poop_happy_05;
            case 5:
                return R.raw.rw_poop_happy_06;
        }
        return R.raw.rw_poop_happy_01;
    }

    public int getSadRes() {

        int rand = new Random().nextInt(7);

        switch (rand) {
            case 0:
                return R.raw.rw_poop_sad_01;
            case 1:
                return R.raw.rw_poop_sad_02;
            case 2:
                return R.raw.rw_poop_sad_03;
            case 3:
                return R.raw.rw_poop_sad_04;
            case 4:
                return R.raw.rw_poop_sad_05;
            case 5:
                return R.raw.rw_poop_sad_06;
        }
        return R.raw.rw_poop_sad_01;
    }

    public int getFartRes() {

        int rand = new Random().nextInt(7);

        switch (rand) {
            case 0:
                return R.raw.rw_fart_01;
            case 1:
                return R.raw.rw_fart_02;
            case 2:
                return R.raw.rw_fart_03;
            case 3:
                return R.raw.rw_fart_04;
            case 4:
                return R.raw.rw_fart_05;
            case 5:
                return R.raw.rw_fart_06;
        }
        return R.raw.rw_fart_01;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        activityOpen = true;
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
    }

    @Override
    protected void onPause() {
        super.onPause();
        activityOpen = false;
    }
}
