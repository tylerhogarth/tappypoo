package za.co.purpleshoe.tappypoo.ui;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import za.co.purpleshoe.tappypoo.R;
import za.co.purpleshoe.tappypoo.activity.MainActivity;
import za.co.purpleshoe.tappypoo.tapgame.TapGame;

/**
 * Created by Tyler Hogarth on 16/06/20.
 */
public class SettingsDialog extends Dialog {

    private SettingsChangeListener callBack;

    @Bind(R.id.dialog_settings_difficulty_spn)
    Spinner difficultySpn;

    @Bind(R.id.dialog_settings_difficulty_desc_tv)
    TextView difficultyDescTv;

    public SettingsDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_settings);
        ButterKnife.bind(this);

        List<String> options = Arrays.asList(context.getResources().getStringArray(R.array.difficulty_array));
        //we use this technique to right align the list items
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(context, R.layout.settings_spinner_text, options);
        spinnerArrayAdapter.setDropDownViewResource(R.layout.settings_spinner_text);
        difficultySpn.setAdapter(spinnerArrayAdapter);

        difficultySpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (callBack != null) {
                    switch (position) {
                        case 0:
                            setDifficultyDescriptionText(TapGame.Difficulty.EASY);
                            callBack.onDifficultyChange(TapGame.Difficulty.EASY);
                            break;

                        case 1:
                            setDifficultyDescriptionText(TapGame.Difficulty.NORMAL);
                            callBack.onDifficultyChange(TapGame.Difficulty.NORMAL);
                            break;

                        case 2:
                            setDifficultyDescriptionText(TapGame.Difficulty.HARD);
                            callBack.onDifficultyChange(TapGame.Difficulty.HARD);
                            break;
                    }
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        if (context instanceof MainActivity) {
            TapGame.Difficulty difficulty = ((MainActivity) context).getTapGame().getDifficulty();

            if (difficulty == TapGame.Difficulty.EASY) {
                difficultySpn.setSelection(0);
            } else if (difficulty == TapGame.Difficulty.NORMAL) {
                difficultySpn.setSelection(1);
            } else if (difficulty == TapGame.Difficulty.HARD) {
                difficultySpn.setSelection(2);
            }
        }
    }

    public void setDifficultyDescriptionText(TapGame.Difficulty difficulty) {
        switch (difficulty) {
            case EASY:
                difficultyDescTv.setText(getContext().getResources().getString(R.string.diff_easy_desc));
                break;

            case NORMAL:
                difficultyDescTv.setText(getContext().getResources().getString(R.string.diff_normal_desc));
                break;

            case HARD:
                difficultyDescTv.setText(getContext().getResources().getString(R.string.diff_hard_desc));
                break;
        }
    }

    @OnClick(R.id.dialog_settings_done_btn)
    void doneClick() {
        dismiss();
    }

    public SettingsDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected SettingsDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        setContentView(R.layout.dialog_settings);
    }

    public void setOnSettingsChangeListener(SettingsChangeListener cb) {
        callBack = cb;
    }

    public interface SettingsChangeListener {

        void onDifficultyChange(TapGame.Difficulty difficulty);
    }
}
