package za.co.purpleshoe.tappypoo.tapgame;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;

import java.util.Random;

import za.co.purpleshoe.tappypoo.R;


/**
 * Created by Tyler Hogarth on 16/06/18.
 */
public class TapGameBasic extends TapGame {

    /**
     * Increases the speed of the game by this amount with each click. Set to 1% here
     */
    public static int DURATION_MULTIPLIER = 30;
    public static int DURATION_START_MULTIPLIER = 0;

    private int score = 0;

    private double pixCircumference = 100;
    private double pixToDegrees = 0;
    private int maxProgress = 100;
    private int startProgress = 50;
    private int minProgressJump = 20;
    private int maxProgressJump = 60;
    private int thumbWidth = 3;
    private int endPadding = 10;

    /**
     * When the game ends, we use a tap blocker to stop the user from accidentally
     * starting a new game when he didn't mean to. Specifically, when the overlay
     * progress goes over the background progress and the user taps too late.
     */
    private boolean tapBlocker = false;
    private boolean goingRight = false;
    private int multiplier = DURATION_START_MULTIPLIER;
    private boolean rotate = false;

    private TapGameBasicListener callBack;

    private Activity activity;
    private CircularSeekBar seekbarOverlay;
    private CircularSeekBar seekbarBackground;
    private ValueAnimator.AnimatorUpdateListener updateListener;
    private ValueAnimator overlayAnimation;

    public TapGameBasic(Activity activity,
                        CircularSeekBar sbOverlay,
                        CircularSeekBar sbBackground) {
        this.activity = activity;
        this.seekbarOverlay = sbOverlay;
        this.seekbarBackground = sbBackground;

        calculateGameMetrics();

        seekbarOverlay.setMax(maxProgress);
        seekbarBackground.setMax(maxProgress);
        seekbarOverlay.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //do nothing
                return true;
            }
        });
        seekbarBackground.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //do nothing
                return true;
            }
        });

        updateListener = new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                if (!playing) {
                    cancelOverlayAnimation();
                    return;
                }
                int animProgress = (Integer) animation.getAnimatedValue();
                if (goingRight && animProgress - thumbWidth > seekbarBackground.getProgress()) {
                    seekbarOverlay.setProgress(seekbarBackground.getProgress() + thumbWidth);
                    tapBlocker = true;
                    endGame();
                    return;
                } else if (!goingRight && animProgress + thumbWidth < seekbarBackground.getProgress()) {
                    seekbarOverlay.setProgress(seekbarBackground.getProgress() - thumbWidth);
                    tapBlocker = true;
                    endGame();
                    return;
                }
                seekbarOverlay.setProgress(animProgress);
            }
        };
    }

    public void cancelOverlayAnimation() {
        if (overlayAnimation != null) {
            overlayAnimation.cancel();
            overlayAnimation = null;
        }
    }

    public void calculateGameMetrics() {
        float density = activity.getResources().getDisplayMetrics().density;
        float totalPx = 360 * density;
        //there is some padding on either side so we need to account for that here (90 is an estimate)
        totalPx -= 90 * density;
        //start angle = -35 and end angle = 35. So we need to remove 70 degrees from the circumference
        //70 degrees is 80.5% so we remove 19.4% from the total
        totalPx -= 0.194 * totalPx;
        pixCircumference = Math.PI * totalPx;

        //290 = 360 degrees - (35 + 35)
        pixToDegrees = pixCircumference / 290;

        thumbWidth = (int) (activity.getResources().getDimension(R.dimen.tap_game_pointer_radius) * 2);
        maxProgress = (int) pixCircumference;
        startProgress = maxProgress / 2;
        minProgressJump = ((int) (maxProgress * 0.15));
        maxProgressJump = ((int) (maxProgress * 0.45));
        endPadding = (int) (thumbWidth * 1.5);
    }

    public void setBackgroundProgress(boolean goRight) {
        int current = seekbarOverlay.getProgress();
        int jumpTo = new Random().nextInt(maxProgressJump - minProgressJump) + 1 + minProgressJump;
        if (goRight) {
            jumpTo = current + jumpTo;
            if (jumpTo > seekbarOverlay.getMax() - endPadding) {
                jumpTo = seekbarOverlay.getMax() - endPadding;
            }
        } else {
            jumpTo = current - jumpTo;
            if (jumpTo < endPadding) {
                jumpTo = endPadding;
            }
        }
        seekbarBackground.setProgress(jumpTo);
    }

    public void animateOverlay(boolean goRight) {
        int distance;
        if (goRight) {
            distance = seekbarOverlay.getMax() - seekbarOverlay.getProgress();
            overlayAnimation = ValueAnimator.ofInt(seekbarOverlay.getProgress(), seekbarOverlay.getMax());
        } else {
            distance = seekbarOverlay.getProgress();
            overlayAnimation = ValueAnimator.ofInt(seekbarOverlay.getProgress(), 0);
        }
        int duration = durationValue(distance);
        overlayAnimation.setDuration(duration);
        overlayAnimation.addUpdateListener(updateListener);
        overlayAnimation.setInterpolator(new LinearInterpolator());
        overlayAnimation.start();
    }

    public int durationValue(int distance) {
        int percent;
        if (distance > 0) {
            percent = (int) (((float) distance / (float) maxProgress) * 100);
        } else {
            percent = 1;
        }
        if (durationType == Duration.ARCADE) {

            return ((Duration.getDuration(durationType) - multiplier) / 100) * percent;
        } else {
            return Duration.getDuration(durationType) / 100 * percent;
        }
    }

    public void reverse(boolean start) {
        callBack.onReverse();
        if (!start) {
            score++;
            callBack.onIncrementScore();
            cancelOverlayAnimation();
            goingRight = !goingRight;
            setBackgroundProgress(goingRight);
        }
        animateOverlay(goingRight);
        incrementMultiplier();

        animateSeekBarRotation();

        callBack.postReverse();
    }

    public void animateSeekBarRotation() {
        if (!rotate) {
            return;
        }
        if (seekbarOverlay.getAnimation() != null) {
            seekbarOverlay.getAnimation().cancel();
        }

        if (seekbarBackground.getAnimation() != null) {
            seekbarBackground.getAnimation().cancel();
        }

        if (difficulty == Difficulty.HARD) {
            int rotation;

            if (goingRight) {
                rotation = seekbarBackground.getProgress() - seekbarOverlay.getProgress();
            } else {
                rotation = seekbarOverlay.getProgress() - seekbarBackground.getProgress();
            }

            int duration = durationValue(rotation);
            rotation = (int) (rotation / pixToDegrees);

            if (!goingRight) {
                rotation = -rotation;
            }
            seekbarOverlay.animate().setDuration(duration).rotationBy(rotation).setInterpolator(new LinearInterpolator());
            seekbarBackground.animate().setDuration(duration).rotationBy(rotation).setInterpolator(new LinearInterpolator());
        }
    }

    public void resetRotation(boolean animate) {
        if (animate) {

            seekbarOverlay.animate().rotation(90).setDuration(250).setInterpolator(new AccelerateDecelerateInterpolator());
            seekbarBackground.animate().rotation(90).setDuration(250).setInterpolator(new AccelerateDecelerateInterpolator());
        } else {
            seekbarOverlay.setRotation(90);
            seekbarBackground.setRotation(90);
        }
    }

    public void incrementMultiplier() {
        if (difficulty == Difficulty.NORMAL || difficulty == Difficulty.HARD) {
            multiplier += DURATION_MULTIPLIER;
        }
    }

    @Override
    public void startGame() {
        callBack.onGameStart();
        if (playing) {
            return;
        }
        playing = true;
        reverse(true);
    }

    @Override
    public void readyGame() {
        callBack.onGameReady();
        gameReady = true;
        score = 0;
        seekbarOverlay.setProgress(startProgress);
        goingRight = new Random().nextInt(2) == 1;
        setBackgroundProgress(goingRight);
    }

    @Override
    public void endGame() {
        callBack.onGameEnd();
        playing = false;
        gameReady = false;
        multiplier = DURATION_START_MULTIPLIER;
    }

    public void tapTouch() {
        if (playing) {
            cancelOverlayAnimation();
            if (goingRight && seekbarOverlay.getProgress() + thumbWidth < seekbarBackground.getProgress()) {
                endGame();
                return;
            } else if (!goingRight && seekbarOverlay.getProgress() - thumbWidth > seekbarBackground.getProgress()) {
                endGame();
                return;
            }
            reverse(false);
        } else if (tapBlocker) {
            tapBlocker = false;
        } else if (gameReady) {
            startGame();
        } else {
            readyGame();
        }
    }

    public void setListener(TapGameBasicListener cb) {
        callBack = cb;
    }

    public interface TapGameBasicListener extends TapGameListener {

        void onReverse();
        void postReverse();
        void onIncrementScore();
    }

    public int getScore() {
        return score;
    }

    public void setRotate(boolean rotate) {
        this.rotate = rotate;
    }
}
