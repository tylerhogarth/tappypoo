package za.co.purpleshoe.tappypoo.util;

import android.content.Context;
import android.content.SharedPreferences;

public class UserPreferences {

    public static final String PREFS = "za.co.purpleshoe.tappypoo";

    private static SharedPreferences sp;
    private static SharedPreferences.Editor spEditor;

    public static String OPTION_PLAY_SOUND = "option_play_sound";                                //boolean

    //0, EASY, 1, NORMAL, 2, HARD
    public static String DIFFICULTY_CURRENT = "difficulty_current";                              //int (position)
    public static String SCORE_TOP_EASY = "score_top_easy";                                      //int
    public static String SCORE_TOP_NORMAL = "score_top_normal";                                  //int
    public static String SCORE_TOP_HARD = "score_top_hard";                                      //int

    /**
     * Sets the values of a shared preference for a specific key.
     *
     * @param c     - Context in which this is called.
     * @param key   - The key of the shared preference to save.
     * @param value - The value to store.
     */
    public static void setString(Context c,
                                 String key,
                                 String value) {

        sp = getSharedPreferences(c);

        spEditor = sp.edit();

        spEditor.putString(key, value);

        spEditor.commit();

        sp = null;
    }

    /**
     * Gets the value of the stored shared preference.
     *
     * @param c   - Context in which this is called.
     * @param key - The key of the shared preference to save.
     * @return        - The stored value.
     */
    public static String getString(Context c,
                                   String key, String defaultValue) {

        sp = getSharedPreferences(c);

        String pref = sp.getString(key, defaultValue);

        sp = null;

        return pref;
    }

    /**
     * Sets the values of a shared preference for a specific key.
     *
     * @param c     - Context in which this is called.
     * @param key   - The key of the shared preference to save.
     * @param value - The value to store.
     */
    public static void setBoolean(Context c,
                                  String key,
                                  boolean value) {

        sp = getSharedPreferences(c);

        spEditor = sp.edit();

        spEditor.putBoolean(key, value);

        spEditor.commit();

        sp = null;
    }

    /**
     * Gets the value of the stored shared preference as boolean.
     *
     * @param c   - Context in which this is called.
     * @param key - The key of the shared preference to save.
     * @return        - The stored value.
     */
    public static boolean getBoolean(Context c,
                                     String key, boolean defaultValue) {

        sp = getSharedPreferences(c);

        boolean pref = sp.getBoolean(key, defaultValue);

        sp = null;

        return pref;
    }

    /**
     * Sets the values of a shared preference for a specific key.
     *
     * @param c     - Context in which this is called.
     * @param key   - The key of the shared preference to save.
     * @param value - The value to store.
     */
    public static void setInteger(Context c,
                                  String key,
                                  int value) {

        sp = getSharedPreferences(c);

        spEditor = sp.edit();

        spEditor.putInt(key, value);

        spEditor.commit();

        sp = null;
    }

    /**
     * Gets the value of the stored shared preference as boolean.
     *
     * @param c   - Context in which this is called.
     * @param key - The key of the shared preference to save.
     * @return        - The stored value.
     */
    public static int getInteger(Context c,
                                 String key, int defualtValue) {

        sp = getSharedPreferences(c);

        int pref = sp.getInt(key, defualtValue);

        sp = null;

        return pref;
    }

    /**
     * Sets the values of a shared preference for a specific key.
     *
     * @param c     - Context in which this is called.
     * @param key   - The key of the shared preference to save.
     * @param value - The value to store.
     */
    public static void setLong(Context c,
                               String key,
                               long value) {

        sp = getSharedPreferences(c);

        spEditor = sp.edit();

        spEditor.putLong(key, value);

        spEditor.commit();

        sp = null;
    }

    /**
     * Gets the value of the stored shared preference as boolean.
     *
     * @param c   - Context in which this is called.
     * @param key - The key of the shared preference to save.
     * @return        - The stored value.
     */
    public static long getLong(Context c,
                               String key, long defualtValue) {

        sp = getSharedPreferences(c);

        long pref = sp.getLong(key, defualtValue);

        sp = null;

        return pref;
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(PREFS, Context.MODE_PRIVATE);
    }
}