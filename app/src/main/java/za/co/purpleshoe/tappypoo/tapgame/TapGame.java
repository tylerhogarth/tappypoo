package za.co.purpleshoe.tappypoo.tapgame;

/**
 * Created by Tyler Hogarth on 16/06/17.
 */
public abstract class TapGame {

    private static int MAX_MILLIS_ANIMATION = 3000;
    private static int MAX_MILLIS_ANIMATION_SURVIVAL = 2000;

    protected boolean gameReady;
    protected boolean playing;

    protected Difficulty difficulty = Difficulty.NORMAL;
    protected Duration durationType = Duration.ARCADE;

    public abstract void readyGame();
    public abstract void startGame();
    public abstract void endGame();

    public enum Duration {
        /**
         * Gets faster with each point
         */
        ARCADE,
        /**
         * Stays the same speed
         */
        SURVIVAL;

        public static int getDuration(Duration duration) {

            switch (duration) {
                case ARCADE:
                    return MAX_MILLIS_ANIMATION;

                case SURVIVAL:
                    return MAX_MILLIS_ANIMATION_SURVIVAL;
            }
            return MAX_MILLIS_ANIMATION;
        }
    }

    public enum Difficulty {
        EASY, NORMAL, HARD
    }

    public interface TapGameListener {
        void onGameReady();
        void onGameStart();
        void onGameEnd();
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public void setDurationType(Duration durationType) {
        this.durationType = durationType;
    }

    public boolean isPlaying() {
        return playing;
    }
}