package za.co.purpleshoe.tappypoo.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import za.co.purpleshoe.tappypoo.TappyPoo;

/**
 * Created by Tyler Hogarth on 16/06/21.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected TappyPoo.Analytics analytics;

    abstract String getScreenName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        analytics = TappyPoo.initAnalytics(getApplication());
        analytics.sendScreen(getScreenName());
    }

    public TappyPoo.Analytics getAnalytics() {
        return analytics;
    }
}